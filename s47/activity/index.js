// The "document" refers to the whole webpage.
// The "querySelector" is used to select a specific object (HTML element) from the document. (webpage)

const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

function event(){
	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value
}


txtFirstName.addEventListener('keyup', event);
txtLastName.addEventListener('keyup', event);